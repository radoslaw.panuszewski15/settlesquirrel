@file:Suppress("LocalVariableName")

package com.semidude.settlesquirrel.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.semidude.settlesquirrel.dao.SettlementDao
import com.semidude.settlesquirrel.dao.UserDao
import com.semidude.settlesquirrel.dao.UserContributionDao
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.model.UserContribution

@Database(entities = [Settlement::class, User::class, UserContribution::class], version = 7)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {

    abstract fun settlementDao(): SettlementDao

    abstract fun userDao(): UserDao

    abstract fun userContributionDao(): UserContributionDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return INSTANCE ?: buildDatabase(context)
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context.applicationContext, AppDatabase::class.java,"app_database")
                .addMigrations(
                    migration_1_2,
                    migration_2_3,
                    migration_3_4,
                    migration_4_5,
                    migration_5_6,
                    migration_6_7
                )
                .build()
        }

        private val migration_1_2 = object: Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE User ADD_ITEM COLUMN password TEXT NOT NULL DEFAULT('')")
            }
        }
        private val migration_2_3 = object: Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE User ADD_ITEM COLUMN email TEXT NOT NULL DEFAULT('')")
            }
        }
        private val migration_3_4 = object: Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("""
                    CREATE TABLE UserSettlementJoin(
                        userId INTEGER NOT NULL,
                        settlementId INTEGER NOT NULL,
                        PRIMARY KEY(userId, settlementId)
                        FOREIGN KEY(userId) REFERENCES User(id),
                        FOREIGN KEY(settlementId) REFERENCES Settlement(id)
                    )""".trimIndent())
            }
        }

        private val migration_4_5 = object: Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE UserSettlementJoin RENAME TO UserContributions")
                database.execSQL("ALTER TABLE UserContributions ADD_ITEM COLUMN contribution INTEGER NOT NULL DEFAULT(0)")
            }
        }

        private val migration_5_6 = object: Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE UserContributions RENAME TO UserContribution")
                database.execSQL("ALTER TABLE UserContribution RENAME TO old_UserContribution")
                database.execSQL("""
                    CREATE TABLE UserContribution(
                        userId INTEGER NOT NULL,
                        settlementId INTEGER NOT NULL,
                        amount INTEGER NOT NULL,
                        PRIMARY KEY(userId, settlementId)
                        FOREIGN KEY(userId) REFERENCES User(id),
                        FOREIGN KEY(settlementId) REFERENCES Settlement(id)
                    )""".trimIndent())
                database.execSQL("""
                    INSERT INTO UserContribution(userId, settlementId, amount)
                    SELECT userId, settlementId, contribution FROM old_UserContribution
                    """.trimIndent())
                database.execSQL("DROP TABLE old_UserContribution")
            }
        }

        private val migration_6_7 = object: Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE UserContribution RENAME TO old_UserContribution")
                database.execSQL("""
                    CREATE TABLE UserContribution(
                        userId INTEGER NOT NULL,
                        settlementId INTEGER NOT NULL,
                        amount INTEGER NOT NULL,
                        PRIMARY KEY(userId, settlementId),
                        FOREIGN KEY(userId) REFERENCES User(id) ON DELETE CASCADE,
                        FOREIGN KEY(settlementId) REFERENCES Settlement(id) ON DELETE CASCADE
                    )""".trimIndent())
                database.execSQL("INSERT INTO UserContribution SELECT * FROM old_UserContribution")
                database.execSQL("DROP TABLE old_UserContribution")
            }
        }
    }
}