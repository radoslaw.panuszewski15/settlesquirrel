package com.semidude.settlesquirrel.db

import androidx.room.TypeConverter
import com.semidude.settlesquirrel.model.SettlementType
import java.math.BigDecimal

class Converters {

    @TypeConverter
    fun fromSettlementType(type: SettlementType): Int
            = type.ordinal

    @TypeConverter
    fun toSettlementType(ordinal: Int): SettlementType
            = SettlementType.values()[ordinal]

    //TODO fix
    //NOTE: We store money in database as INTEGER type, so we convert it to lowest common unit.
    //So, for example, for dollars and cents, we store it as cents ($1.25 will be stored as 125)
    @TypeConverter
    fun moneyToLowestUnit(bigDecimal: BigDecimal): Long
            = bigDecimal.longValueExact()

    @TypeConverter
    fun lowestUnitToMoney(value: Long): BigDecimal
            = BigDecimal(value)
}