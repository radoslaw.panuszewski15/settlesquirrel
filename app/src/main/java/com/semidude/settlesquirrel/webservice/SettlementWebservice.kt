package com.semidude.settlesquirrel.webservice

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

data class Greeting(
    var id: Long,
    var content: String
)

interface SettlementWebservice {
    @GET("/greeting")
    fun getGreeting(@Query("name") name: String): Call<Greeting>
}