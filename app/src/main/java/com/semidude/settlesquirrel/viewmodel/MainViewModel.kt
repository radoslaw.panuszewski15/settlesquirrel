package com.semidude.settlesquirrel.viewmodel

import android.app.Application
import com.semidude.settlesquirrel.model.*
import com.semidude.settlesquirrel.repository.SettlementRepository
import com.semidude.settlesquirrel.repository.UserContributionRepository
import com.semidude.settlesquirrel.repository.UserRepository
import com.semidude.settlesquirrel.util.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.Subject
import io.sellmair.disposer.Disposer
import io.sellmair.disposer.disposeBy
import java.math.BigDecimal
import java.util.*

class MainViewModel
constructor(
    private val settlementRepo: SettlementRepository,
    private val userRepo: UserRepository,
    private val contributionRepo: UserContributionRepository,
    override val context: Application
): ViewModelWithDisposers(), ContextHolder {

    private val hiddenItemSubject: Subject<Settlement> =
        BehaviorSubject.create()

    init {
        hiddenItemSubject.onNext(DUMMY_SETTLEMENT)
    }

    /**
     * Current list of settlements to be displayed in UI.
     * Doesn't have to match actual data in database (for example one settlement could be temporary hidden in UI.
     */
    val settlementsObservable: Observable<List<Settlement>> =
        Observable.combineLatest(
            settlementRepo.getSettlements(),
            hiddenItemSubject,
            { list, hiddenItem -> list - hiddenItem }
        )

    /**
     * Current logged user
     */
    val currentUserObservable: Observable<User> =
        userRepo.findUserById(preferences.getLong("currentUserId", 0))

    /**
     * Add new settlement to database
     * @param settlement the settlement to be added
     * @param onComplete optional callback called when operation completes
     */
    fun addSettlement(settlement: Settlement, onComplete: () -> Unit = {}) {
        settlementRepo.addSettlement(settlement)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onComplete)
            .disposeBy(onCleared)
    }

    /**
     * Update settlement in database
     * @param settlement the settlement to be updated
     * @param onComplete optional callback called when operation completes
     */
    fun updateSettlement(settlement: Settlement, onComplete: () -> Unit = {}) {
        settlementRepo.updateSettlement(settlement)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onComplete)
            .disposeBy(onCleared)
    }

    /**
     * Delete settlement from database
     * @param settlement the settlement to be deleted
     * @param onComplete optional callback called when operation completes
     */
    fun deleteSettlement(settlement: Settlement, onComplete: () -> Unit = {}) {
        val disposer = Disposer()

        settlementRepo.deleteSettlement(settlement)
            .andThen(settlementsObservable)
            .subscribeOn(Schedulers.io())
            .subscribe {
                onComplete()
                //at this time, the hidden item is already deleted, but we have to restore the normal state
                restoreHiddenSettlement()
                disposer.dispose()
            }
            .disposeBy(disposer)
    }

    /**
     * Hide settlement in UI-displayed list, but don't delete it from database yet
     * @param settlement  the settlement to be hidden
     */
    fun hideSettlement(settlement: Settlement) {
        hiddenItemSubject.onNext(settlement)
    }

    /**
     * Restore hidden settlement if it hasn't been deleted or simply restore normal state of list if it has
     */
    fun restoreHiddenSettlement() {
        hiddenItemSubject.onNext(DUMMY_SETTLEMENT)
    }

    /**
     * Get list of calculated debts in given settlement
     * @param settlement the settlement to calculate debts from
     * @return list of calculated debts
     */
    fun getCalculatedDebts(settlement: Settlement): Single<List<Debt>> {
        return contributionRepo.findContributionsInSettlement(settlement)
            .observeOn(Schedulers.computation())
            .map { contributions ->
                computeDebts(settlement, contributions)
            }
    }

    private fun computeDebts(settlement: Settlement, contributions: List<UserContribution>): MutableList<Debt> {
        //TODO refactor
        val debts = mutableListOf<Debt>()
        val userBalances = nonNullMutableMapOf<User, BigDecimal>(BigDecimal.ZERO)
        val users: Map<Long, User> =
            userRepo.findMembersOfSettlement(settlement)
                .blockingGet()
                .map { user -> user.id to user }
                .toMap()
        val pricePerMember = settlement.price / users.size.toBigDecimal()

        contributions.forEach { contribution ->
            userBalances[users[contribution.userId]!!] = contribution.amount
        }

        val underPaymentUsers = users.values.filter { userBalances[it] < pricePerMember }
        val excessPaymentUsers = users.values.filter { userBalances[it] > pricePerMember }
        val underPaymentQueue: Deque<User> = LinkedList(underPaymentUsers)

        for (receiver in excessPaymentUsers) {
            var excess = userBalances[receiver] - pricePerMember

            while (excess > BigDecimal.ZERO) {
                val debtor = underPaymentQueue.removeFirst()
                val debtAmount = min(excess, pricePerMember)
                debts.add(Debt(debtor, receiver, debtAmount))
                userBalances[debtor] = userBalances[debtor] + debtAmount
                userBalances[receiver] = userBalances[receiver] - debtAmount

                if (userBalances[debtor] < pricePerMember)
                    underPaymentQueue.addFirst(debtor)

                excess = userBalances[receiver] - pricePerMember
            }
        }

        return debts
    }
}