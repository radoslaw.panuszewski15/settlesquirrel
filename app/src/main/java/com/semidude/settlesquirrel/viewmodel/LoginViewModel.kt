package com.semidude.settlesquirrel.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.repository.UserRepository
import com.semidude.settlesquirrel.util.ContextHolder
import com.semidude.settlesquirrel.util.ViewModelWithDisposers
import io.reactivex.Maybe
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.MaybeSubject
import io.sellmair.disposer.disposeBy

class LoginViewModel
constructor(
    private val userRepo: UserRepository,
    override val context: Application
): ViewModelWithDisposers(), ContextHolder {

    fun loginUser(email: String, password: String): Maybe<User> {
        return userRepo.findUserByEmail(email)
            .flatMap { user ->
                if (user.password == password)
                    Maybe.just(user)
                else
                    Maybe.empty()
            }
    }
}