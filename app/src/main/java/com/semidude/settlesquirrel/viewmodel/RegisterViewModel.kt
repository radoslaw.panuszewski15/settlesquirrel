package com.semidude.settlesquirrel.viewmodel

import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.repository.UserRepository
import com.semidude.settlesquirrel.util.ViewModelWithDisposers
import io.reactivex.Completable
import io.reactivex.Maybe

class RegisterViewModel
constructor(
    private val userRepo: UserRepository
): ViewModelWithDisposers() {

    fun registerUser(user: User): Completable {
        //TODO probably some data validation would be good idea
        return userRepo.createUser(user)
    }
}