package com.semidude.settlesquirrel.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.adapter.SettlementsAdapter
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.SettlementType
import com.semidude.settlesquirrel.util.*
import com.semidude.settlesquirrel.viewmodel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class ListFragment: Fragment() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var settlementsAdapter: SettlementsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        createAdapter()
        bindObservableData()
    }

    private fun createAdapter() {
        settlementsAdapter = SettlementsAdapter(parentActivity).apply {
            onItemClickListener = ::showDetails
            onItemLongClickListener = { settlement, _ -> showDeleteDialog(settlement) }
        }
    }

    private fun bindObservableData() {
        viewModel.settlementsObservable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { items ->
                settlementsAdapter.refreshItems(items)
            }
            .disposeBy(onDestroy)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupFab()
    }

    private fun setupRecyclerView() {
        settlementsRecyclerView.apply {
            adapter = settlementsAdapter
            layoutManager = LinearLayoutManager(parentActivity, RecyclerView.VERTICAL, false)
        }

        settlementsRecyclerView.addSwipeToDeleteSupport<SettlementsAdapter.ViewHolder>
            { swipedPosition -> deleteSettlementWithUndo(settlementsAdapter[swipedPosition]) }
    }

    private fun showDeleteDialog(itemToDelete: Settlement): Boolean {
        AlertDialog.Builder(parentActivity)
            .setMessage("Do you want to delete this item?")
            .setPositiveButton("Yes") { _, _ ->
                deleteSettlementWithUndo(itemToDelete)
            }
            .setNegativeButton("No") { _, _ -> }
            .create()
            .show()
        return true
    }

    private fun deleteSettlementWithUndo(itemToDelete: Settlement) {
        viewModel.hideSettlement(itemToDelete)

        var undoClicked = false
        Snackbar
            .make(fragmentListRootView, "'${itemToDelete.name}' deleted", Snackbar.LENGTH_LONG)
            .setAction("UNDO") {
                undoClicked = true
                viewModel.restoreHiddenSettlement()
            }
            .doOnDismissed {
                if (!undoClicked)
                    viewModel.deleteSettlement(itemToDelete)
            }
            .show()
    }

    private fun showDetails(settlement: Settlement, viewHolder: SettlementsAdapter.ViewHolder) {
        fragmentManager!!
            .beginTransaction()
            .addSharedElement(viewHolder.foregroundView, BACKGROUND_PREFIX + settlement.name)
            .addSharedElement(viewHolder.nameView, NAME_PREFIX + settlement.name)
            .addSharedElement(viewHolder.iconView, ICON_PREFIX + settlement.name)
            .addSharedElement(viewHolder.priceView, PRICE_PREFIX + settlement.name)
            .addToBackStack(TAG)
            .replace(R.id.contentContainer, DetailsFragment.newInstance(settlement))
            .commit()
    }

    private fun setupFab() {
        addSettlementButton.setOnClickListener {
            val settlement = Settlement(0,"witam", SettlementType.PIZZA, 30.toBigDecimal())
            viewModel.addSettlement(settlement)
        }
    }

    companion object {
        val TAG: String = ListFragment::class.java.simpleName
        fun newInstance() = ListFragment()
    }
}