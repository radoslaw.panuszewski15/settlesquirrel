package com.semidude.settlesquirrel.fragment

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.adapter.DebtsAdapter
import com.semidude.settlesquirrel.util.parentActivity
import com.semidude.settlesquirrel.model.findIconResource
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.util.*
import com.semidude.settlesquirrel.viewmodel.MainViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.fragment_details.*
import org.koin.android.viewmodel.ext.android.viewModel

class DetailsFragment: Fragment() {

    private val viewModel: MainViewModel by viewModel()
    private lateinit var debtsAdapter: DebtsAdapter
    private lateinit var settlement: Settlement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        settlement = arguments?.getParcelable(EXTRA_SETTLEMENT) as Settlement
        createAdapter()
        //TODO zrobić tak, żeby adapter miał itemy przed rozpoczęciem animacji (cache?)
        fetchMembers(settlement)
        setupEnterTransition()
    }

    private fun createAdapter() {
        debtsAdapter = DebtsAdapter(parentActivity)
    }

    private fun setupEnterTransition() {
        sharedElementEnterTransition = TransitionInflater
            .from(context)
            .inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews(settlement)
        setupRecyclerView()
    }

    private fun setupViews(settlement: Settlement) {
        backgroundView.transitionName = BACKGROUND_PREFIX + settlement.name
        nameView.transitionName = NAME_PREFIX + settlement.name
        iconView.transitionName = ICON_PREFIX + settlement.name
        priceView.transitionName = PRICE_PREFIX + settlement.name

        nameView.text = settlement.name
        priceView.text = settlement.price.toString() + " zł"
        iconView.setImageResource(settlement.findIconResource())
    }

    private fun setupRecyclerView() {
        membersRecyclerView.apply {
            adapter = debtsAdapter
            layoutManager = LinearLayoutManager(parentActivity, RecyclerView.VERTICAL, false)
        }
    }

    private fun fetchMembers(settlement: Settlement) {
        viewModel.getCalculatedDebts(settlement)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { debts ->
                debtsAdapter.refreshItems(debts)
            }
            .disposeBy(onDestroy)
    }

    companion object {
        fun newInstance(settlement: Settlement): DetailsFragment {
            val fragment = DetailsFragment()
            fragment.arguments = Bundle().apply {
                putParcelable(EXTRA_SETTLEMENT, settlement)
            }
            return fragment
        }
    }
}
