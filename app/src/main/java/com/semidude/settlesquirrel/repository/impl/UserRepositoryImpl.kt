package com.semidude.settlesquirrel.repository.impl

import android.database.sqlite.SQLiteConstraintException
import com.semidude.settlesquirrel.dao.UserContributionDao
import com.semidude.settlesquirrel.dao.UserDao
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.model.UserContribution
import com.semidude.settlesquirrel.repository.UserRepository
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import kotlin.concurrent.thread

class UserRepositoryImpl
constructor(
    private val userDao: UserDao,
    private val contributionDao: UserContributionDao
): UserRepository {

    init {
        thread {
            //just for testing purposes
            try {
                userDao.insert(User(123, "a", "a", "a", "a"))
            }
            catch(e: SQLiteConstraintException) {

            }
        }
    }

    //TODO implement in-memory cache

    override fun findUserById(id: Long): Observable<User> {
        return userDao.findUserById(id)
    }

    override fun findUserByEmail(email: String): Maybe<User> {
        return userDao.findUserByEmail(email)
    }

    override fun findMembersOfSettlement(settlement: Settlement): Maybe<List<User>> {
        return userDao.findMembersOfSettlement(settlement.id)
    }

    override fun findContributionsInSettlement(settlement: Settlement): Single<List<UserContribution>> {
        return contributionDao.findContributionsInSettlement(settlement.id)
    }

    override fun createUser(user: User): Completable {
        return Completable.fromAction {
            userDao.insert(user)
        }
    }
}