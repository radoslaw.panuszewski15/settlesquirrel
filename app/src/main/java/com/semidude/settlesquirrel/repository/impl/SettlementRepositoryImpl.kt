package com.semidude.settlesquirrel.repository.impl

import android.database.sqlite.SQLiteConstraintException
import com.semidude.settlesquirrel.dao.SettlementDao
import com.semidude.settlesquirrel.dao.UserDao
import com.semidude.settlesquirrel.dao.UserContributionDao
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.SettlementType
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.model.UserContribution
import com.semidude.settlesquirrel.repository.SettlementRepository
import com.semidude.settlesquirrel.webservice.SettlementWebservice
import io.reactivex.Completable
import io.reactivex.Observable
import kotlin.concurrent.thread

class SettlementRepositoryImpl
constructor (
    private val settlementDao: SettlementDao,
    private val userDao: UserDao,
    private val joinDao: UserContributionDao,
    private val webservice: SettlementWebservice
): SettlementRepository {

    init {
        thread {
            //just for testing purposes
            try {
//                settlementDao.insert(Settlement(1, "Pizza w piątek", SettlementType.PIZZA, 30.toBigDecimal()))
                settlementDao.insert(Settlement(2, "Impreza", SettlementType.ALCOHOL, 80.toBigDecimal()))
//                settlementDao.insert(Settlement(3, "Kino Avengers: Endgame", SettlementType.CINEMA, 50.toBigDecimal()))

//                userDao.insert(User(id = 1, firstName = "Radek"))
//                userDao.insert(User(id = 2, firstName = "Olaf"))
//                userDao.insert(User(id = 3, firstName = "Ania"))
//                userDao.insert(User(id = 4, firstName = "Maciek"))
//                userDao.insert(User(id = 5, firstName = "Marcin"))
//                userDao.insert(User(id = 6, firstName = "Kamil"))
//                userDao.insert(User(id = 7, firstName = "Sebastian"))
//                userDao.insert(User(id = 8, firstName = "Konrad"))

//                joinDao.insert(UserContribution(1, 1, 5.toBigDecimal()))
//                joinDao.insert(UserContribution(2, 1, 0.toBigDecimal()))
//                joinDao.insert(UserContribution(3, 1, 25.toBigDecimal()))
                joinDao.insert(UserContribution(4, 2, 60.toBigDecimal()))
                joinDao.insert(UserContribution(5, 2, 20.toBigDecimal()))
//                joinDao.insert(UserContribution(6, 3, 10.toBigDecimal()))
//                joinDao.insert(UserContribution(7, 3, 10.toBigDecimal()))
//                joinDao.insert(UserContribution(8, 3, 30.toBigDecimal()))
            }
            catch(e: SQLiteConstraintException) {
                e.printStackTrace()
            }
        }
    }

    override fun getSettlements(): Observable<List<Settlement>> {
        return settlementDao.getAll()
    }

    override fun addSettlement(settlement: Settlement): Completable {
        return Completable.fromAction {
            settlementDao.insert(settlement)
        }
    }

    override fun updateSettlement(settlement: Settlement): Completable {
        return Completable.fromAction {
            settlementDao.update(settlement)
        }
    }

    override fun deleteSettlement(settlement: Settlement): Completable {
        return Completable.fromAction {
            settlementDao.delete(settlement)
        }
    }
}