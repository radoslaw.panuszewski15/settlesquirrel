package com.semidude.settlesquirrel.repository

import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.webservice.Greeting
import io.reactivex.Completable
import io.reactivex.Observable

interface SettlementRepository {

    fun getSettlements(): Observable<List<Settlement>>
    fun addSettlement(settlement: Settlement): Completable
    fun updateSettlement(settlement: Settlement): Completable
    fun deleteSettlement(settlement: Settlement): Completable
}