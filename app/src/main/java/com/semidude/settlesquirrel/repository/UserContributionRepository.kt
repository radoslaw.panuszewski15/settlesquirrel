package com.semidude.settlesquirrel.repository

import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.UserContribution
import io.reactivex.Single

interface UserContributionRepository {

    fun findContributionsInSettlement(settlement: Settlement): Single<List<UserContribution>>
}