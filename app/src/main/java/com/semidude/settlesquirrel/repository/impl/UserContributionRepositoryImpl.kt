package com.semidude.settlesquirrel.repository.impl

import com.semidude.settlesquirrel.dao.UserContributionDao
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.UserContribution
import com.semidude.settlesquirrel.repository.UserContributionRepository
import io.reactivex.Single

class UserContributionRepositoryImpl
constructor(
    val dao: UserContributionDao
): UserContributionRepository {

    override fun findContributionsInSettlement(settlement: Settlement): Single<List<UserContribution>> {
        return dao.findContributionsInSettlement(settlement.id)
    }
}