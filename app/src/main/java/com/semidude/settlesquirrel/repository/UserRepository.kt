package com.semidude.settlesquirrel.repository

import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.model.UserContribution
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

interface UserRepository {
    fun findUserById(id: Long): Observable<User>
    fun findUserByEmail(email: String): Maybe<User>
    fun findMembersOfSettlement(settlement: Settlement): Maybe<List<User>>
    fun findContributionsInSettlement(settlement: Settlement): Single<List<UserContribution>>
    fun createUser(user: User): Completable
}
