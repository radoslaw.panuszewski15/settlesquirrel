package com.semidude.settlesquirrel.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.model.Debt
import com.semidude.settlesquirrel.util.ContextHolder
import com.semidude.settlesquirrel.util.BaseAdapter

class DebtsAdapter
constructor(
    context: Context
): BaseAdapter<DebtsAdapter.ViewHolder, Debt>(context), ContextHolder {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflateView(R.layout.member_list_item, parent)
        return ViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bindDebt(items[position])
    }

    inner class ViewHolder
    constructor(
        itemView: View
    ): RecyclerView.ViewHolder(itemView) {

        val debtorNameView: TextView = itemView.findViewById(R.id.memberNameView)
        val receiverNameView: TextView = itemView.findViewById(R.id.memberNameView2)
        val statusView: TextView = itemView.findViewById(R.id.memberStatusView)

        fun bindDebt(debt: Debt) {
            debtorNameView.text = debt.debtor.firstName
            receiverNameView.text = debt.receiver.firstName
            statusView.text = "owes ${debt.amount} zł to"
        }
    }
}