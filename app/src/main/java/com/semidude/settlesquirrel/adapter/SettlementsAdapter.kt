package com.semidude.settlesquirrel.adapter

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.model.findIconResource
import com.semidude.settlesquirrel.model.Settlement
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.util.*

class SettlementsAdapter
constructor (
    context: Context
): BaseAdapter<SettlementsAdapter.ViewHolder, Settlement>(context) {

    override fun doCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflateView(R.layout.settlement_list_item, parent)
        return ViewHolder(itemView)
    }

    override fun doBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val settlement = items[position]
        viewHolder.bindSettlement(settlement)

        viewHolder.apply {
            foregroundView.transitionName = BACKGROUND_PREFIX + settlement.name
            nameView.transitionName = NAME_PREFIX + settlement.name
            iconView.transitionName = ICON_PREFIX + settlement.name
            priceView.transitionName = PRICE_PREFIX + settlement.name
        }
    }

    inner class ViewHolder
    constructor (
        itemView: View
    ): RecyclerView.ViewHolder(itemView), HavingForegroundAndBackground {

        override val foregroundView: ConstraintLayout = itemView.findViewById(R.id.settlementItemForeground)
        override val backgroundView: ConstraintLayout = itemView.findViewById(R.id.settlementItemBackground)
        val iconView: ImageView = itemView.findViewById(R.id.settlementIconView)
        val nameView: TextView = itemView.findViewById(R.id.settlementNameView)
        val membersView: TextView = itemView.findViewById(R.id.settlementMembersView)
        val priceView: TextView = itemView.findViewById(R.id.settlementPriceView)

        val relatedItem: Settlement get() = this@SettlementsAdapter[adapterPosition]

        fun bindSettlement(settlement: Settlement) {
            iconView.setImageResource(settlement.findIconResource())
            nameView.text = settlement.name
            membersView.text = settlement.members.map(User::firstName).joinToString(", ")
            priceView.text = settlement.price.toString() + " zł"
        }
    }

}