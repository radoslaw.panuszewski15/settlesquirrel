package com.semidude.settlesquirrel.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.util.removeCurrentFocus
import com.semidude.settlesquirrel.util.showToast
import com.semidude.settlesquirrel.util.startActivity
import com.semidude.settlesquirrel.viewmodel.LoginViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)
        removeCurrentFocus()
        setupButtons()
    }

    private fun setupButtons() {
        createAccountButton.setOnClickListener {
            startActivity<RegisterActivity>()
        }

        loginButton.setOnClickListener {
            val email = emailText.text.toString()
            val password = passwordText.text.toString()

            viewModel.loginUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribeBy(
                    onSuccess = { startActivity<MainActivity>() },
                    onComplete = { showToast("Login failed, check your email and/or password") },
                    onError = { showToast("Something went really wrong, please try again later") }
                )
                .disposeBy(onDestroy)
        }
    }
}