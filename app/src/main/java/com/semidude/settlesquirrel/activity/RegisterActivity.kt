package com.semidude.settlesquirrel.activity

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.util.removeCurrentFocus
import com.semidude.settlesquirrel.util.showToast
import com.semidude.settlesquirrel.util.startActivity
import com.semidude.settlesquirrel.viewmodel.RegisterViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.sellmair.disposer.disposeBy
import io.sellmair.disposer.onDestroy
import kotlinx.android.synthetic.main.activity_register.*
import org.koin.android.viewmodel.ext.android.viewModel

class RegisterActivity: AppCompatActivity() {

    private val viewModel: RegisterViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        setSupportActionBar(toolbar)
        removeCurrentFocus()
        setupButtons()
    }

    private fun setupButtons() {
        goToLoginButton.setOnClickListener {
            startActivity<LoginActivity>()
        }

        registerButton.setOnClickListener {
            val user = createUserFromInput()
                ?: return@setOnClickListener

            viewModel.registerUser(user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(Throwable::printStackTrace)
                .subscribeBy(
                    onComplete = { showSuccessfulDialog() },
                    onError = { showToast("Sorry, we've encountered a problem while registration") }
                )
                .disposeBy(onDestroy)
        }
    }

    private fun createUserFromInput(): User? {
        val email = emailText.text.toString()
        val firstName = firstNameText.text.toString()
        val lastName = lastNameText.text.toString()
        val password = passwordText.text.toString()
        val repeatedPassword = repeatPasswordText.text.toString()

        if (repeatedPassword != password) {
            showToast("Passwords don't match, please correct them")
            return null
        }

        return User(0, email, firstName, lastName, password)
    }

    private fun showSuccessfulDialog() {
        AlertDialog.Builder(this)
            .setMessage("Account created successfully! Do you want to login now?")
            .setPositiveButton("Yes") { _, _ ->
                startActivity<LoginActivity>()
            }
            .setNegativeButton("No") { _, _ ->
                clearInput()
                removeCurrentFocus()
            }
            .create()
            .show()
    }

    private fun clearInput() {
        emailText.setText("")
        firstNameText.setText("")
        lastNameText.setText("")
        passwordText.setText("")
        repeatPasswordText.setText("")
    }
}
