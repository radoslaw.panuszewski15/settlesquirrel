package com.semidude.settlesquirrel.app

import android.app.Application
import com.semidude.settlesquirrel.di.appModule
import org.koin.android.ext.android.startKoin

@Suppress("Unused")
class SettleSquirrelApplication: Application() {

    companion object {
        lateinit var INSTANCE: SettleSquirrelApplication
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
        startKoin(this, listOf(appModule))
    }
}