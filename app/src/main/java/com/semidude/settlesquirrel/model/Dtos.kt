package com.semidude.settlesquirrel.model

import java.math.BigDecimal

data class Debt(
    val debtor: User,
    val receiver: User,
    val amount: BigDecimal
)