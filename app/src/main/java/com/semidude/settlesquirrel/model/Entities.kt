package com.semidude.settlesquirrel.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Ignore
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal

@Entity
@Parcelize
data class User(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val email: String = "",
    val firstName: String = "",
    val lastName: String = "",
    val password: String = ""
): Parcelable


@Entity
@Parcelize
data class Settlement(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    var name: String = "",
    var type: SettlementType = SettlementType.PIZZA,
    var price: BigDecimal = 0.toBigDecimal(),
    @Ignore var members: MutableList<User> = mutableListOf()
): Parcelable


@Entity(
    primaryKeys = [ "userId", "settlementId" ],
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = [ "id" ],
            childColumns = [ "userId" ],
            onDelete = CASCADE
        ),
        ForeignKey(
            entity = Settlement::class,
            parentColumns = [ "id" ],
            childColumns = [ "settlementId" ],
            onDelete = CASCADE
        )
    ]
)
data class UserContribution(
    var userId: Long,
    var settlementId: Long,
    var amount: BigDecimal
)

val DUMMY_SETTLEMENT = Settlement(id = -1)