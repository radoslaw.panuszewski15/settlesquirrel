package com.semidude.settlesquirrel.model

enum class SettlementType {
    PIZZA,
    RESTAURANT,
    CINEMA,
    SHOPPING,
    GIFT,
    BEER,
    ALCOHOL
}