package com.semidude.settlesquirrel.model

import com.semidude.settlesquirrel.R
import com.semidude.settlesquirrel.model.SettlementType.*

fun Settlement.findIconResource(): Int =
    when(type) {
        PIZZA -> R.drawable.ic_pizza
        RESTAURANT -> R.drawable.ic_restaurant
        CINEMA -> R.drawable.ic_cinema
        SHOPPING -> R.drawable.ic_shopping
        GIFT -> R.drawable.ic_gift
        BEER -> R.drawable.ic_beer
        ALCOHOL -> R.drawable.ic_alcohol
    }