@file:Suppress("RemoveExplicitTypeArguments")

package com.semidude.settlesquirrel.di

import com.semidude.settlesquirrel.dao.SettlementDao
import com.semidude.settlesquirrel.dao.UserDao
import com.semidude.settlesquirrel.dao.UserContributionDao
import com.semidude.settlesquirrel.db.AppDatabase
import com.semidude.settlesquirrel.repository.SettlementRepository
import com.semidude.settlesquirrel.repository.UserContributionRepository
import com.semidude.settlesquirrel.repository.UserRepository
import com.semidude.settlesquirrel.repository.impl.SettlementRepositoryImpl
import com.semidude.settlesquirrel.repository.impl.UserContributionRepositoryImpl
import com.semidude.settlesquirrel.repository.impl.UserRepositoryImpl
import com.semidude.settlesquirrel.viewmodel.LoginViewModel
import com.semidude.settlesquirrel.viewmodel.MainViewModel
import com.semidude.settlesquirrel.viewmodel.RegisterViewModel
import com.semidude.settlesquirrel.webservice.SettlementWebservice
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    viewModel<LoginViewModel> { LoginViewModel(get(), get()) }

    viewModel<RegisterViewModel> { RegisterViewModel(get()) }

    viewModel<MainViewModel> { MainViewModel(get(), get(), get(), get()) }

    single<UserRepository> { UserRepositoryImpl(get(), get()) }

    single<SettlementRepository> { SettlementRepositoryImpl(get(), get(), get(), get()) }

    single<UserContributionRepository> { UserContributionRepositoryImpl(get()) }

    single<AppDatabase> { AppDatabase.getInstance(androidContext()) }

    single<UserDao> { get<AppDatabase>().userDao() }

    single<SettlementDao> { get<AppDatabase>().settlementDao() }

    single<UserContributionDao> { get<AppDatabase>().userContributionDao() }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    single<SettlementWebservice> { get<Retrofit>().create(SettlementWebservice::class.java) }
}