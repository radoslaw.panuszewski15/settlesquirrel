package com.semidude.settlesquirrel.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.semidude.settlesquirrel.model.UserContribution
import io.reactivex.Single

@Dao
interface UserContributionDao {

    @Query("SELECT * FROM UserContribution")
    fun getAll(): Single<List<UserContribution>>

    @Insert
    fun insert(userContribution: UserContribution)

    @Query("SELECT * FROM UserContribution uc INNER JOIN Settlement s ON uc.settlementId=s.id WHERE s.id=:settlementId")
    fun findContributionsInSettlement(settlementId: Long): Single<List<UserContribution>>
}
