package com.semidude.settlesquirrel.dao

import androidx.room.*
import com.semidude.settlesquirrel.model.Settlement
import io.reactivex.Observable

@Dao
interface SettlementDao {

    @Query("SELECT * FROM Settlement")
    fun getAll(): Observable<List<Settlement>>

    @Insert
    fun insert(settlement: Settlement)

    @Update
    fun update(settlement: Settlement)

    @Delete
    fun delete(settlement: Settlement)
}