package com.semidude.settlesquirrel.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.semidude.settlesquirrel.model.User
import com.semidude.settlesquirrel.model.UserContribution
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getAll(): Single<List<User>>

    @Query("SELECT * FROM User WHERE id = :id")
    fun findUserById(id: Long): Observable<User>

    @Query("SELECT * FROM User WHERE firstName = :email")
    fun findUserByEmail(email: String): Maybe<User>

    @Query("SELECT u.id, u.email, u.firstName, u.lastName, u.password FROM User u INNER JOIN UserContribution uc ON u.id = uc.userId INNER JOIN Settlement s ON uc.settlementId = s.id WHERE s.id = :settlementId")
    fun findMembersOfSettlement(settlementId: Long): Maybe<List<User>>

    @Insert
    fun insert(user: User)

    @Delete
    fun delete(user: User)
}