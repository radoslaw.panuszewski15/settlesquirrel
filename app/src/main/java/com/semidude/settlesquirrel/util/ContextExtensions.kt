package com.semidude.settlesquirrel.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

interface ContextHolder {
    val context: Context
    val preferences: SharedPreferences get() = context.preferences

    fun inflateView(resource: Int, parent: ViewGroup): View =
        LayoutInflater.from(context)
            .inflate(resource, parent, false)
}

val Context.preferences: SharedPreferences
    get() = PreferenceManager.getDefaultSharedPreferences(this)

fun Context.showToast(message: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, length).show()
}