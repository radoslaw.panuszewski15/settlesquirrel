package com.semidude.settlesquirrel.util

import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar

fun Snackbar.doOnDismissed(callback: () -> Unit): Snackbar {
    this.addCallback(object: BaseTransientBottomBar.BaseCallback<Snackbar>() {
        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
            callback()
        }
    })
    return this
}