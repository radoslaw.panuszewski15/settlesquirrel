package com.semidude.settlesquirrel.util

import java.math.BigDecimal

fun min(first: BigDecimal, second: BigDecimal): BigDecimal {
    return first.min(second)
}