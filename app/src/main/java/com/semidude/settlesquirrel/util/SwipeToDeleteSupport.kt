@file:Suppress("UNCHECKED_CAST")

package com.semidude.settlesquirrel.util

import android.graphics.Canvas
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

interface HavingForegroundAndBackground {
    val foregroundView: View
    val backgroundView: View
}

class SwipeToDeleteSupport<TViewHolder>: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT)
where
  TViewHolder: RecyclerView.ViewHolder,
  TViewHolder: HavingForegroundAndBackground {

    var onSwipedListener: (Int) -> Unit = {}

    fun attachToRecyclerView(recyclerView: RecyclerView) {
        ItemTouchHelper(this).attachToRecyclerView(recyclerView)
    }

    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        return true
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder != null) {
            viewHolder.backgroundView.visibility = View.VISIBLE
            getDefaultUIUtil().onSelected(viewHolder.foregroundView)
        }
    }

    override fun onChildDraw(c: Canvas,
                             recyclerView: RecyclerView,
                             viewHolder: RecyclerView.ViewHolder,
                             dX: Float,
                             dY: Float,
                             actionState: Int,
                             isCurrentlyActive: Boolean)
    {
        getDefaultUIUtil().onDraw(c, recyclerView, viewHolder.foregroundView, dX, dY, actionState, isCurrentlyActive)
    }

    override fun onChildDrawOver(c: Canvas,
                                 recyclerView: RecyclerView,
                                 viewHolder: RecyclerView.ViewHolder,
                                 dX: Float,
                                 dY: Float,
                                 actionState: Int,
                                 isCurrentlyActive: Boolean)
    {
        getDefaultUIUtil().onDrawOver(c, recyclerView, viewHolder.foregroundView, dX, dY, actionState, isCurrentlyActive)
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        getDefaultUIUtil().clearView(viewHolder.foregroundView)
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        onSwipedListener(viewHolder.adapterPosition)
    }

    private val RecyclerView.ViewHolder.foregroundView: View get() = (this as TViewHolder).foregroundView
    private val RecyclerView.ViewHolder.backgroundView: View get() = (this as TViewHolder).backgroundView
}

fun <TViewHolder> RecyclerView.addSwipeToDeleteSupport(onSwipedListener: (Int) -> Unit)
where
  TViewHolder: RecyclerView.ViewHolder,
  TViewHolder: HavingForegroundAndBackground {

    SwipeToDeleteSupport<TViewHolder>().apply { this.onSwipedListener = onSwipedListener }
        .attachToRecyclerView(this)
}