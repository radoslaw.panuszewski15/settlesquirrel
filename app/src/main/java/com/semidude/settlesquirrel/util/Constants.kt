package com.semidude.settlesquirrel.util

const val EXTRA_SETTLEMENT = "settlement"

const val BACKGROUND_PREFIX = "background-"
const val NAME_PREFIX = "name-"
const val ICON_PREFIX = "icon-"
const val PRICE_PREFIX = "price-"