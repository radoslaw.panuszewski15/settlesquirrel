package com.semidude.settlesquirrel.util

import android.content.Context
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Base class for RecyclerView's adapters, just for convenience
 */
abstract class BaseAdapter<TViewHolder: RecyclerView.ViewHolder, TItem>
constructor (
    override val context: Context
): RecyclerView.Adapter<TViewHolder>(), ContextHolder {

    /**
     * Collection of adapted items
     */
    protected var items = listOf<TItem>()

    /**
     * Callback for item click
     */
    var onItemClickListener: (TItem, TViewHolder) -> Unit = {_, _ -> }

    /**
     * Callback for item long click
     */
    var onItemLongClickListener: (TItem, TViewHolder) -> Boolean = {_, _ -> false}

    /**
     * Operator []
     */
    operator fun get(i: Int) = items[i]

    /**
     * Refresh data set, not suitable for large data sets
     */
    fun refreshItems(newItems: List<TItem>) {
        val oldItems = items
        items = newItems

        if (newItems.size > oldItems.size)
            newItems
                .filterNot { oldItems.contains(it) }
                .forEach { addedItem -> notifyItemInserted(newItems.indexOf(addedItem)) }

        else if (newItems.size < oldItems.size)
            oldItems
                .filterNot { newItems.contains(it) }
                .forEach { removedItem -> notifyItemRemoved(oldItems.indexOf(removedItem)) }
    }

    /**
     * Simpler version of [refreshItems], no additional list analysis is done. Potentially faster than [refreshItems]
     */
    fun loadItems(newItems: List<TItem>) {
        items = newItems
        notifyDataSetChanged()
    }

    /**
     * Remove item from adapter, use it instead of [refreshItems] if you care about performance
     */
    fun removeItem(item: TItem) {
        val position = items.indexOf(item)
        items = items.filterNot { it == item }
        notifyItemRemoved(position)
    }

    /**
     * * Add item to adapter, use it instead of [refreshItems] if you care about performance
     */
    fun addItem(item: TItem) {
        items = items + item
        notifyItemInserted(items.indexOf(item))
    }

    /**
     * Called when new MemberViewHolder instance needs to be created
     * Override it instead of [onCreateViewHolder]
     * @param parent The ViewGroup into which the new View will be added after it is bound to
     *               an adapter position.
     * @param viewType The view type of the new View.
     * @return A new MemberViewHolder that holds a View of the given view type
     */
    protected abstract fun doCreateViewHolder(parent: ViewGroup, viewType: Int): TViewHolder

    /**
     * Called when item needs to be bound to MemberViewHolder
     * Override it instead of [onBindViewHolder]
     * @param viewHolder The MemberViewHolder which should be updated to represent the contents of the
     *                   item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    protected abstract fun doBindViewHolder(viewHolder: TViewHolder, position: Int)


    //==================================================================================================================


    final override fun getItemCount() = items.size

    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TViewHolder {
        return doCreateViewHolder(parent, viewType)
    }

    final override fun onBindViewHolder(viewHolder: TViewHolder, position: Int) {
        doBindViewHolder(viewHolder, position)

        viewHolder.itemView.setOnClickListener {
            onItemClickListener(items[viewHolder.adapterPosition], viewHolder)
        }

        viewHolder.itemView.setOnLongClickListener {
            onItemLongClickListener(items[viewHolder.adapterPosition], viewHolder)
        }
    }
}