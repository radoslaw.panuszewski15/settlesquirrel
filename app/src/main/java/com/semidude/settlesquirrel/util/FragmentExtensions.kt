package com.semidude.settlesquirrel.util

val androidx.fragment.app.Fragment.parentActivity: androidx.fragment.app.FragmentActivity
    get() = this.activity ?:
        throw IllegalStateException("Fragment ${this} isn't attached to activity but accesses it")