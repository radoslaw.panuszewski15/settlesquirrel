package com.semidude.settlesquirrel.util

class NonNullMap<K, V>
constructor(
    private val map: Map<K, V>,
    private val defaultValue: V
): Map<K, V> by map {

    override operator fun get(key: K): V {
        return map[key] ?: defaultValue
    }
}

class NonNullMutableMap<K, V>
constructor(
    private val map: MutableMap<K, V>,
    private val defaultValue: V
): MutableMap<K, V> by map {

    override operator fun get(key: K): V {
        return map[key] ?: defaultValue
    }
}

fun <K, V> Map<K, V>.nonNull(defaultValue: V): NonNullMap<K, V> {
    return NonNullMap(this, defaultValue)
}

fun <K, V> nonNullMapOf(defaultValue: V, vararg pairs: Pair<K, V> = emptyArray()): NonNullMap<K, V> {
    return mapOf(*pairs).nonNull(defaultValue)
}

fun <K, V> MutableMap<K, V>.nonNull(defaultValue: V): NonNullMutableMap<K, V> {
    return NonNullMutableMap(this, defaultValue)
}

fun <K, V> nonNullMutableMapOf(defaultValue: V, vararg pairs: Pair<K, V> = emptyArray()): NonNullMutableMap<K, V> {
    return mutableMapOf(*pairs).nonNull(defaultValue)
}