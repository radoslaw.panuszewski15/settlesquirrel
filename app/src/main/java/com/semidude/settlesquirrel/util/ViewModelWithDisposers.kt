package com.semidude.settlesquirrel.util

import androidx.lifecycle.ViewModel
import io.sellmair.disposer.Disposer

open class ViewModelWithDisposers: ViewModel() {

    protected val onCleared = Disposer.create()

    override fun onCleared() {
        super.onCleared()
        onCleared.dispose()
    }
}